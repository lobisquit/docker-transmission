FROM crazymax/alpine-s6

# setup the user
ARG PUID=1000
RUN adduser -u ${PUID} -h /var/lib/transmission -D transmission

RUN apk add --no-cache transmission-daemon transmission-cli tzdata && \
    rm -rf /var/cache/apk/*

# copy the setup code for s6 and transmission
COPY app/ /

# expose these volumes
VOLUME /var/lib/transmission/config/
VOLUME /var/lib/transmission/downloads/
VOLUME /var/lib/transmission/incomplete/
VOLUME /var/lib/transmission/watched/

# expose all needed ports
EXPOSE 9091
EXPOSE 52437 52437/udp

ENTRYPOINT ["/init"]
